(function($) {
	"use strict"; 
	
	var hH;
	
	$(document).ready(function() {
		hH = $("header").height() + 70;
		$('.menu-mobile-toggle').on( "click", function(){
			if ($('.navbar-main').hasClass('active'))
				$('.navbar-main').removeClass('active');
			else
				$('.navbar-main').addClass('active');
		});
		$('.navbar-main .close-btn').click(function(){
			$('.navbar-main').removeClass('active');
			$('#navbar').removeClass('in');
		});
		mega_menu();
		$(".flex-news").flexslider({
			controlNav: false,
			directionNav: false
		});
		mega_mobile();
		if ($(".custom-heapbox").length){
			$('.custom-heapbox').heapbox({
                'onChange':function(){
                    $('.filter select.custom-heapbox').trigger('change');
                },
            });
		}
		$(".swatch-list li").each(function(){
			var color = $(this).find("span").css("background-color");
			if (color == "rgb(255, 255, 255)"){
				$(this).addClass("white-option");
			}
		});
		
		if ($(".tab-plugin").length){
			tab_plugin();
		}
		
		$(".plus-btn").on("click",function(){
			var input = $(this).parent().find("input").first();
			input.val(parseInt(input.val(),10) + 1);
			return false;
		});
		$(".minus-btn").on("click",function(){
			var input = $(this).parent().find("input").first();
			if (input.val() > 1){
				input.val(parseInt(input.val(),10) - 1);
			}
			return false;
		});
		
		modal_process();
		
		$(".btn-search-toggle").on("click",function(){
			$("#menu-fixed-top .searchform").toggleClass("active");
		});
		$("#wrapper").on("click",function(event){
			if ($(event.target).hasClass("btn-search-toggle") || $(event.target).hasClass("searchform") || $(event.target).parents(".searchform").length || $(event.target).parents(".btn-search-toggle").length){
				
			}else{
				$("#menu-fixed-top .searchform").removeClass("active");
			}
		});
		
		$("#menu-fixed-bottom .header-top .flex-slider").flexslider({
			animation: "slide",
			direction: "vertical",
			controlNav: false,
			directionNav: false,
			minItems: 1,
			maxItems: 1,
			itemMargin: 20
		});
		if ($(window).width() < 992){
			$(".woocommerce-main-image img").elevateZoom({
				constrainType:"height", 
				constrainSize:274,
				scrollZoom: true,
				zoomType: "lens",
				gallery	: 'comfythemes_thumb_image'
			});
			
		}else{
			$(".woocommerce-main-image img").elevateZoom({
				gallery	: 'comfythemes_thumb_image',
				scrollZoom: true, 
			});
		}
		$(".woocommerce-main-image").addClass("loading");
		var load_img_html = '<div class="hidden">';
		
		var counter = 0;
		var max = $("#comfythemes_thumb_image a").length;
		$("#comfythemes_thumb_image a").each(function(){
			if ($(this).attr("data-zoom-image")){
				var loadingImage = new Image();
				loadingImage.onload = function(){
					counter++;
					console.log(counter);
					if (counter >= max){
						$(".woocommerce-main-image").removeClass("loading");
					}
				};
				loadingImage.src = $(this).attr("data-zoom-image");
			}
		});
		
		if ($(".inp--kendo").length){
			$(".inp--kendo").each(function(){
				var tId = $(this).attr("id");
				var nId = $(this).data("target");
				if (nId){
					$(nId).datetimepicker({
						altField: "#" + tId,
						altFieldTimeOnly: false,
						showSecond: false,
						showMillisec: false,
						showMicrosec: false,
						showTimezone: false
					});
					
					$("#" + tId).on("click",function(){
						$(".inp--datetime-container").removeClass("active");
						$(nId).addClass("active");
					});
					$("#wrapper").bind("click",function(event){
						var current = $(event.target);
						if (current.hasClass("inp--datetime-container") || current.parents(".inp--datetime-container").length || current.hasClass("inp--kendo")){
							// do nothing
						}else{
							$(".inp--datetime-container").removeClass("active");
						}
					});
				}else{
					$("#" + tId).datetimepicker();
				}
			});
			
		}
		
		$(".label-toggle input").on("change",function(){
			var current = $(this).parents(".label-toggle").next(".toggle-content");
			current.toggleClass("active");
		});
		print_window();
	});
	
	$('body').on('graphflow_get_recs_returned', function() {
		if ($(".owl-slider").length){
			$(".owl-slider").each(function(){
				var current = $(this);
				if (current.hasClass("owl-slider-half")){
					current.owlCarousel({
						navigation: true,
						pagination: false,
						rewindNav: false,
						items : 2,
						itemsDesktop : [699,1]
					});
				} else {
					current.owlCarousel({
						navigation: true,
						pagination: false,
						rewindNav: false,
						items : 3,
						itemsDesktop: [746,2],
						itemsDesktopSmall : [699,1]
					});
				}
			});
		}
	});
	
	$(window).bind("load",function(){
		if ($(".owl-slider").length){
			$(".owl-slider").each(function(){
				var current = $(this);
				if (current.hasClass("owl-slider-half")){
					current.owlCarousel({
						navigation: true,
						pagination: false,
						rewindNav: false,
						items : 2,
						itemsDesktop : [699,1]
					});
				} else {
					current.owlCarousel({
						navigation: true,
						pagination: false,
						rewindNav: false,
						items : 3,
						itemsDesktop: [746,2],
						itemsDesktopSmall : [699,1]
					});
				}
			});
		}
		
		center_product();
		sticky_sidebar($(".sticky_sidebar"),0,992);
	});
	
	$(window).on("resize",function(){
		hH = $("header").height() + 70;
		if ($(".thumbnails").length){
			ver_change_control();
		}
		center_product();
		sticky_sidebar($(".sticky_sidebar"),0,992);
	});
	$(window).on("scroll",function(){
		var yW = window.scrollY;
		if ((yW > hH) && (hH > 0)){
			$("#menu-fixed-top").addClass("active");
			$("#menu-fixed-bottom").addClass("active");
		}else{
			$("#menu-fixed-top").removeClass("active");
			$("#menu-fixed-bottom").removeClass("active");
		}
		sticky_sidebar($(".sticky_sidebar"),0,992);
	});
	$(window).on("load",function(){
		if ($(".product .images .thumbnails img").length){
			$('<div class="control-thumbnails"><div class="control-down control"></div><div class="control-up control"></div></div>').insertAfter(".product .images .thumbnails");
			ver_slider()
		}
	});
	
	function print_window(){
		$(".so-quy-table").on("click",".button-print",function(){
			var tParent = $(this).parents(".box-in-table"),
				tId = tParent.find(".id-ticket").text(),
				tDate = tParent.find(".date-ticket").text(),
				tPrice = tParent.find(".price-ticket").text(),
				tPriceText = docso(Number(tPrice.replace(/[^0-9\.]+/g,"")));
			$("#prinable-area .id-ticket").text(tId);
			$("#prinable-area .price-ticket").text(tPrice);
			$("#prinable-area .price-text-ticket").text(toTitleCase(tPriceText.trim()));
			$("#prinable-area .date-ticket").text(tDate);
			window.print();
		});
	}
	
	function toTitleCase(str){
		console.log(str);
		console.log(str.charAt(0));
		return str.charAt(0).toUpperCase() + str.slice(1);
	}
	
	var mangso = ['không','một','hai','ba','bốn','năm','sáu','bảy','tám','chín'];
	function dochangchuc(so,daydu){
		var chuoi = "";
		var chuc = Math.floor(so/10);
		var donvi = so%10;
		if (chuc>1) {
			chuoi = " " + mangso[chuc] + " mươi";
			if (donvi==1) {
				chuoi += " mốt";
			}
		} else if (chuc==1) {
			chuoi = " mười";
			if (donvi==1) {
				chuoi += " một";
			}
		} else if (daydu && donvi>0) {
			chuoi = " lẻ";
		}
		if (donvi==5 && chuc>1) {
			chuoi += " lăm";
		} else if (donvi>1||(donvi==1&&chuc==0)) {
			chuoi += " " + mangso[ donvi ];
		}
		return chuoi;
	}
	function docblock(so,daydu){
		var chuoi = "";
		var tram = Math.floor(so/100);
		so = so%100;
		if (daydu || tram>0) {
			chuoi = " " + mangso[tram] + " trăm";
			chuoi += dochangchuc(so,true);
		} else {
			chuoi = dochangchuc(so,false);
		}
		return chuoi;
	}
	function dochangtrieu(so,daydu){
		var chuoi = "";
		var trieu = Math.floor(so/1000000);
		so = so%1000000;
		if (trieu>0) {
			chuoi = docblock(trieu,daydu) + " triệu";
			daydu = true;
		}
		var nghin = Math.floor(so/1000);
		so = so%1000;
		if (nghin>0) {
			chuoi += docblock(nghin,daydu) + " nghìn";
			daydu = true;
		}
		if (so>0) {
			chuoi += docblock(so,daydu);
		}
		return chuoi;
	}
	function docso(so){
		if (so==0) return mangso[0];
		var chuoi = "", hauto = "",ty = 0;
		do {
			ty = so%1000000000;
			so = Math.floor(so/1000000000);
			if (so>0) {
				chuoi = dochangtrieu(ty,true) + hauto + chuoi;
			} else {
			chuoi = dochangtrieu(ty,false) + hauto + chuoi;
			}
			hauto = " tỷ";
		} while (so>0);
		return chuoi;
	}
	
	function sticky_sidebar(current,start,limit){
		var wW = $(window).width();
		var target = current.data("target");
		if (wW > limit && target){
			var h_target = $(target).outerHeight() - 130,
				t_target = $(target).offset().top - 35,
				h_current = current.outerHeight(),
				t_current = current.offset().top - 35,
				t_window = window.scrollY;
				if (t_window > t_target && (t_window < (t_target + h_target) - h_current)){
					current.css({
						"margin-top" : start + (t_window - t_target)
					});
				}else{
					if (t_window < t_target){
						current.css({
							"margin-top" : start
						});
					}else{
						current.css({
							"margin-top" : start + h_target - h_current
						});
					}
				}
			
		}else{
			removeCss(current,'margin-top')
		}
	}
	
	function removeCss(current,name){
		var myCss = current.attr('css');
		if (myCss){
			myCss = myCss.replace(name + ': ' + current.css(name)+';', '');
			if(myCss == '') {
				current.removeAttr('css');
			} else {
				current.attr('css', myCss);
			}
		}
	}
	
	function center_product(){
		if($(".product-item-shop .overlay").length){
			$(".product-cats .product-item-shop .overlay").each(function(){
				var t_top = $(this).height();
				var p_top = $(this).parents(".product-item-shop").find("img").first().height();
				$(this).css({
					"top" : (p_top - t_top) / 2
				});
			});
		}
	}
	
	function tab_plugin(){
		var counter = 0;
		$(".tab-plugin").each(function(){
			var current = $(this);
			var dToggle = current.data("toggle");
			var dShow = current.data("show");
			counter++;
			current.addClass("tab-plugin-" + counter);
			$(this).on("click",".tab-control",function(event){
				event.preventDefault();
				var target = $(this).data("target");
				var prevToggle;
				if (dShow != "all"){
					if ($(this).hasClass("tab-active")){
						prevToggle = true;
					}else{
						prevToggle = false;
					}
					current.find(".tab-active").removeClass("tab-active");//Remove all active
					if (dToggle == "yes"){// Restore current
						if (prevToggle == true){ 
							$(this).toggleClass("tab-active");
							if (target){
								$(target).toggleClass("tab-active");
							}else{
								var index = current.find('.tab-control').index($(this));
								current.find('.tab-content').eq(index).toggleClass("tab-active");
							}
						}
					}
				}
				if (dToggle == "yes"){
					$(this).toggleClass("tab-active");
				}else{
					$(this).addClass("tab-active");
				}
				if (target){
					if (dToggle == "yes"){
						$(target).toggleClass("tab-active");
					}else{
						$(target).addClass("tab-active");
					}
				}
				else{
					var index = current.find('.tab-control').index($(this));
					if (dToggle == "yes"){
						current.find('.tab-content').eq(index).toggleClass("tab-active");
					}else{
						current.find('.tab-content').eq(index).addClass("tab-active");
					}
				}
			});
		});
		if (counter > 0){
			$("#wrapper").bind('click',function(event){
				var selector = $(event.target);
				for (var i = 1;i <= counter;i++){
					var dOutside = $(".tab-plugin-" + i).data("outside");
					if (dOutside == "yes"){
						if (!(selector.hasClass("tab-plugin-" + i) || selector.parents(".tab-plugin-" + i).length)){
							$(".tab-plugin-" + i).find(".tab-active").removeClass("tab-active");
						}
					}
				}
			});
		}
	}
	
	
	function ver_slider(){
		var ver_couter = 0;
		$(".product .images .thumbnails").each(function(){ 
			var current = $(this);//sCurrent
			var parent = current.parents(".images"); // sParent
			var control = parent.find(".control-thumbnails");
			var num = current.children().length;
			var max = current.height() / num; // Get height Child For Scroll
			max = (parent.height() - control.outerHeight()) / max; // Get number visible
			max = Math.floor(max); // Round downward max 1.4 -> 1
			max = num - max; // Get max base on visible item
			max++; // Fix max for last item always show
			ver_couter++;
			current.attr("id","vertical-slider-" + ver_couter);
			current.data("max",max);
			current.data("current",1);
			parent.on("click",".control-down",function(){
				ver_moveSlide(current,"down");
			});
			parent.on("click",".control-up",function(){
				ver_moveSlide(current,"up");
			});
			
			// Touch for mobile
			var hammertime = new Hammer(document.getElementById("vertical-slider-" + ver_couter));
			hammertime.on("panup touchstart",function(){
				ver_moveSlide(current,"up");
			});
			hammertime.on("pandown touchstart",function(){
				ver_moveSlide(current,"down");
			});
		});
	}
	
	function ver_change_control(){ //Show/hide Control
		$(".product .images .thumbnails").each(function(){ //sCurrent
			var current = $(this);//sCurrent
			var parent = current.parents(".images"); // sParent
			var control = parent.find(".control-thumbnails");
			var num = current.children().length;
			var max = current.height() / num; // Get height Child For Scroll
			max = (parent.height() - control.outerHeight()) / max; // Get number visible
			max = Math.floor(max); // Round downward max 1.4 -> 1
			max = num - max; // Get max base on visible item
			max++; // Fix max for last item always show
			current.data("max",max);
			var hChild = $(this).height() / $(this).children(); // Get height Child For Scroll
			if ($(this).height <= parent.height()){
				control.hide();
			}
		});
	}
	
	function ver_moveSlide(current,direction){
		var parent = current.parents(".images"); // sParent
		var dCurrent = current.data("current");
		var max = current.data("max");
		if (dCurrent < 1 || dCurrent == "") dCurrent = 1;
		if (dCurrent > max) dCurrent = max;
		if (direction == "up"){
			parent.find(".control-down").removeClass("control-disable");
			if (dCurrent < max){// if not last
				dCurrent++;
				ver_moveSlide_child(current,dCurrent);
			}
			if (dCurrent == max) parent.find(".control-up").addClass("control-disable"); // if bottom -> disable it
		}else{
			parent.find(".control-up").removeClass("control-disable");
			if (dCurrent > 1){// if not first
				dCurrent--;
				ver_moveSlide_child(current,dCurrent);
			}
			if (dCurrent == 1) parent.find(".control-down").addClass("control-disable"); // if top -> disable it
		}
	}
	
	function ver_moveSlide_child(selector,position){
		var hChild = selector.height() / selector.children().length; // Get height Child For Scroll
		selector.css({
			"top" : - (hChild * (position - 1))
		});
		selector.data("current",position);//store after slide
	}
	
	
	function mega_menu(){
		$("#navbar > .menu > .menu-item-has-children").not(".mega-menu-wrap").addClass("no-mega-menu");
		$("#navbar .mega-menu-wrap").each(function(){
			var mega = $(this).children(".sub-menu");
			var counter = 0;
			var hasImg = false;
			mega.addClass("mega-menu");
			mega.children("li").each(function(){
				$(this).children("a").addClass("title");
				if ($(this).find("img").length){
					$(this).addClass("mega-img");
					$(this).addClass("col-sm-4");
					$(this).addClass("col-ossvn");
					hasImg = true;
				}else{
					counter++;
					$(this).addClass("mega-no-img");
					$(this).append('<a href="' + $(this).children("a").attr("href") + '" class="view-all"><i class="fa fa-angle-double-right"></i> XEM TOÃ€N Bá»˜</i></a>');
				}
			});
			if (hasImg){
				mega.addClass("mega-has-img");
				switch(counter){
					case 1: counter = 8;break;
					case 2: counter = 4;break;
					case 3: counter = 31;break;
					default: counter = 2;break;
				}
			}else{
				if (counter > 4){
					counter = 3;
				}else{
					counter = 12 / counter;
				}
			}
			mega.children(".mega-no-img").addClass("col-sm-" + counter);
			mega.children(".mega-no-img").addClass("col-ossvn");
		});
	}
	
	function mega_mobile(){
		$("#navbar .menu-item-has-children > a").on("click",function(event){
			var wW = $(window).width();
			if (wW < 769){
				if ($(this).next(".sub-menu").length){
					event.preventDefault();
					if ($(this).parent().hasClass("active")){
						$(this).parent().removeClass("active");
					}else{
						$("#navbar .active").removeClass("active");
						$(this).parents(".menu-item-has-children").addClass("active");
					}
					
				}
			}	
		});
		
	};
	
	function modal_process(){
		$(".button-guest-checkout").on("click",function(){
			$(".modal--input,.modal--inputed").hide();
			$(".modal--guest").show();
			$('#guest-checkout-modal').attr('data-createorder', 'guest');
			$( document.body ).trigger( 'comfythemes_empty_cart' );
		});
		$(".guest_table_wrap").on("click",".button-create-order",function(){
			$(".modal--input,.modal--guest").hide();
			$(".modal--inputed").show();
			$("#save--guest .inp-name").val($(this).parents("tr").find(".guest-name").text());
			$("#save--guest .inp-date").val($(this).parents("tr").find(".guest-date").text());
			$("#save--guest .inp-tel").val($(this).data("tel"));
			$("#save--guest .inp-id").val($(this).parents("tr").find(".guest-id").val());
			$("#save--guest .inp-address").val($(this).parents("tr").find(".guest-address").text());
			$('#guest-checkout-modal').attr('data-createorder', 'customer');
			$( document.body ).trigger( 'comfythemes_empty_cart' );
		});
		$(".guest_message").on("click",".button-create-guest",function(){
			$(".modal--inputed,.modal--guest").hide();
			$(".modal--input").show();
			$("#create--guest .inp-name").val("");
			$("#create--guest .inp-date").val("");
			$("#create--guest .inp-tel").val("");
			$('#guest-checkout-modal').attr('data-createorder', 'customer_create');
			$( document.body ).trigger( 'comfythemes_empty_cart' );
		});
	}

	/**
    * AJAX Email
    */
    $('.widget').on( 'click', '.contact-form input[type="submit"]', function(){

		var $this = $(this),
		name      = $('input[name="contact_name"]').val(),
		email     = $('input[name="contact_email"]').val(),
		subj     = $('input[name="contact_subject"]').val(),
		comment   = $('.input-form[name="contact_comment"]').val(),
		_status   = true,
		$data     = { action: 'comfytheme_send_email', contact_name: name, contact_email: email, contact_subject: subj, contact_msg: comment };

		if( name == '' ){
			$('input[name="contact_name"]').addClass('error');
		}else{
			$('input[name="contact_name"]').removeClass('error');
		}

		if( email == '' ){
			$('input[name="contact_email"]').addClass('error');
		}else{
			$('input[name="contact_email"]').removeClass('error');
		}

		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

		if ( ! pattern.test( email  ) ) {
			$('input[name="contact_email"]').addClass('error');
		}else{
			$('input[name="contact_email"]').removeClass('error');
		}

		if( comment == '' ){
			$('.input-form[name="contact_comment"]').addClass('error');
		}else{
			$('.input-form[name="contact_comment"]').removeClass('error');
		}

		if( name != '' && email != '' && comment != '' && pattern.test( email  ) ){

			$.ajax({
			    type       : "POST",
			    data       : $data,
			    url        : comfythemes_9_params.ajax_url,
			    beforeSend : function(){
			        
			        $this.val(comfythemes_9_params.sending_text);
			    },
			    success    : function(respon){
			      
			      var data = $.parseJSON(respon);

			      if( data.status == 1 ){
			        $this.val(comfythemes_9_params.send_success);
			        $('.input-form[name="contact_comment"]').val('');
			      }else{
			      	$this.val(comfythemes_9_params.send_error);
			      }

			      setTimeout(function(){
			          $this.val(comfythemes_9_params.submit_email);
			      }, 2000);

			    },
			    error     : function(jqXHR, textStatus, errorThrown) {
			        alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
			    }

			});

		}

		return false;

    });
})(jQuery);